import React, { Component } from 'react';
import classNames from 'classnames';

var bannerImg = require('./img/BannerWin_V3.jpg');
var arrowRight = require('../../img/ui/arrow-right.png');

export default class Splash extends Component {

	static contextTypes = {
		state: React.PropTypes.object,
		dispatch: React.PropTypes.func
	};

	startExperience = () => {
		this.context.dispatch({type: 'START'});
	};

	render() {

		let classes = classNames({
			'invisible': this.context.state.hasOnboarded,
			'section': true,
		});

		return (
		  <div className={classes}>
			  <div className="line-header header-container">
				  <h1 className="lang-en">STUDENT STYLE-OFF</h1>
			  </div>

			  <img src={bannerImg} className="winBanner img-en center-block" />

			  <a className="start lang-en btn" onClick={this.startExperience}>
				  Create your look
				  <img className="btn__icon" src={arrowRight} alt=""/>
			  </a>
		  </div>
		);
	}
}