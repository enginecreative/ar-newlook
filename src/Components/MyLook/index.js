import React, { Component } from 'react';
import classNames from 'classnames';

const facebookLogo = require('../../img/ui/facebook.png');
const twitterLogo = require('../../img/ui/twitter.png');
const arrowRight = require('../../img/ui/arrow-right.png');

export default class MyLook extends Component {

	static contextTypes = {
		state: React.PropTypes.object,
		dispatch: React.PropTypes.func
	};

	launchGarmentSelector = garment => {
		this.context.dispatch({type: 'LAUNCH_GARMENT', garment})
	};

	enterCompetition = () => {

	};

	shareLook = socialNetwork => {
		this.context.dispatch({type: 'SHARE', platform: socialNetwork});
	};

	render() {

		let classes = classNames({
			'invisible': !this.context.state.hasOnboarded,
			'section': true,
			'look': true
		});

		return (
		  <div className={classes}>
			  <div className="line-header header-container">
				  <h1 className="lang-en">Create Your Look</h1>
			  </div>

			  <figure className="row">
				  <aside className="cell" data-category="jackets" data-title="Jackets" onClick={this.launchGarmentSelector.bind(this, 'jackets')}>
					  <img src={this.context.state.jackets ? `img/${this.context.state.jackets.image}` : 'img/placeholder_jackets.png'} className="center-block" />
					  <p className="lang-en">{this.context.state.jackets ? this.context.state.jackets.title : 'Choose a Jacket'}</p>

					  <a className="clear lang-en">clear</a>
				  </aside>
				  <aside className="cell" data-category="tops" data-title="Tops" onClick={this.launchGarmentSelector.bind(this, 'tops')}>
					  <img src={this.context.state.tops ? `img/${this.context.state.tops.image}` : 'img/placeholder_tops.png'} className="center-block" />
					  <p className="lang-en">{this.context.state.tops ? this.context.state.tops.title : 'Choose a Top'}</p>

					  <a className="clear lang-en">clear</a>
				  </aside>
			  </figure>
			  <figure className="row">
				  <aside className="cell" data-category="bottoms" data-title="Bottoms" onClick={this.launchGarmentSelector.bind(this, 'bottoms')}>
					  <img src={this.context.state.bottoms ? `img/${this.context.state.bottoms.image}` : 'img/placeholder_bottoms.png'} className="center-block" />
					  <p className="lang-en">{this.context.state.bottoms ? this.context.state.bottoms.title : 'Choose Bottoms'}</p>

					  <a className="clear lang-en">clear</a>
				  </aside>
				  <aside className="cell" data-category="accessories" data-title="Accessories" onClick={this.launchGarmentSelector.bind(this, 'accessories')}>
					  <img src={this.context.state.accessories ? `img/${this.context.state.accessories.image}` : 'img/placeholder_accessories.png'}  className="center-block" />
					  <p className="lang-en">{this.context.state.accessories ? this.context.state.accessories.title : 'Choose an Accessory'}</p>

					  <a className="clear lang-en">clear</a>
				  </aside>
			  </figure>

			  <nav className="row">
				  <div className="cell">
					  <a className="share lang-en btn btn-blue" onClick={this.shareLook}>Share <span className="btn__icon"><img src={facebookLogo} alt="Share on Facebook"/></span> <span className="btn__icon"><img src={twitterLogo} alt="Share on Twitter"/></span></a>
				  </div>
				  <div className="cell">
					  <a className="enterToWin lang-en btn" onClick={this.enterCompetition}>Enter to Win <span className="btn__icon btn__icon--small"><img src={arrowRight} alt="Enter to win"/></span></a>
				  </div>
			  </nav>

			  <aside id="invalidLookPopover" className={classNames({'hidden': this.context.state.isValidLook, 'popover-background': true})}>
				  <div id="invalidLookContents">
					  <p className="lang-en">Please pick three items to complete your look.</p>

					  <a className="closeInvalidLook lang-en btn">OK <span className="btn__icon"><img src={arrowRight} alt="OK"/></span></a>
				  </div>
			  </aside>
		  </div>
		);
	}
}