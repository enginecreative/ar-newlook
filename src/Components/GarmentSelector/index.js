import React, { Component } from 'react';
import classNames from 'classnames';

// Import content
import content from './content';

import rightArrow from '../../img/ui/arrow-right.png';

// Find which global this exposes and use es6 imports
const Slider = require('react-slick');

export default class GarmentSelector extends Component {

	static contextTypes = {
		state: React.PropTypes.object,
		dispatch: React.PropTypes.func
	};

	addGarmentToLook = (garments) => {

		let index = document.querySelector('.slick-active').getAttribute('data-index');
		let garment = garments[index];

		this.context.dispatch({
			type: 'ADD_GARMENT',
			garment,
			garmentType: this.context.state.garmentType
		});
	};

	render() {

		let classes = classNames({
			'invisible': !this.context.state.isSelectingGarment,
			'carousel section': true
		});

		let carouselSettings = {
			dots: true,
			infinite: true,
			speed: 500,
			slidesToShow: 1,
			slidesToScroll: 1,
		};

		let garments = null;

		if (this.context.state.garmentType) {
			garments = content[this.context.state.garmentType];
		}

		return (
		  <div className={classes}>
			  <div className="header-container">
				  <h1>{this.context.state.garmentType}</h1>
			  </div>

			  {garments &&
			  <Slider {...carouselSettings}>
				  {garments.map((item) => {
					  return (
						<div>
							<img src={`img/${item.image}`} alt=""/>
							<p>{item.title}</p>
						</div>
					  );
				  })}
			  </Slider>
			  }

			  <a className="addToMyLook lang-en btn" onClick={this.addGarmentToLook.bind(this, garments)}>Add to My Look <span><img className="btn__icon btn__icon--small" src={rightArrow} alt=""/></span></a>
		  </div>
		);
	}
}