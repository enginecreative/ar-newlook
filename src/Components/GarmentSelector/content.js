const content = {
	tops: [
		{ title: "Denim Shirt", title_ar: "قميص الجينز", image: "tops/325765040_LI-SS15-DENIM-SHIRT.jpg" },
		{ title: "Sleeveless Shirt", title_ar: "قميص بلا أكمام", image: "tops/326472185_F-SS15-MEMPHIS-SL-GDAD-SHIRT.jpg" },
		{ title: "Crochet Hem Cami", title_ar: "هيم الكروشيه", image: "tops/330214501_F-CROCHET-HEM-CAMI.jpg" },
		{ title: "Khaki Wrap Top", title_ar: "كاكي توب", image: "tops/330996234_SLESS-SHAWL-WRAP-FRT-SHELL.jpg" },
		{ title: "Gypsy Top", title_ar: "توب الجيبسي", image: "tops/332501611_LSLV-COLD-SHOULDER-GYPSY.jpg" },
		{ title: "Print Pleated Playsuit", title_ar: "بلايسوت مطوي", image: "tops/333085559_EZRA-TILE-HI-NECK-PSUIT.jpg" },
		{ title: "Aztec Print Playsuit", title_ar: "ازتيك بلايسوت", image: "tops/333910599_AZTEC-PRINT-SPORTY-SL-PSUIT.jpg" },
		{ title: "Abstract Print Jumpsuit", title_ar: "مونوكروم جامبسوت", image: "tops/333912509_HIGH-NECK-MONO-JUMPSUIT.jpg" },
		{ title: "Lace Wide Sleeve Top", title_ar: "توب دانتيل", image: "tops/335068510_COTTON-LACE-PONCHO.jpg" },
		{ title: "Daisy Print Gypsy Top", title_ar: "توب الجيبسي بالطباعة", image: "tops/336428709_RETRO-DAISY-CHELSEY-GYPSY.jpg" },
		{ title: "Stripe Bodycon", title_ar: "بديكون مونوكروم", image: "tops/337999609_PHILLIP-STRIPE-12-SLV-HNK-86.jpg" },
		{ title: "Crop Top", title_ar: "توب قصير", image: "tops/339373701_D-SSLV-RIB-BARDOT-CROP.jpg" },
		{ title: "Strappy Wrap Bodycon", title_ar: "بديكون جريء", image: "tops/339976960_GO-T-ROCHELLE-WRAP-SKIRT-BCON.jpg" },
		{ title: "Strappy Lace Bodycon", title_ar: "بديكون دانتيل", image: "tops/341142401_GO-EYELASH-LACE-BCON-DRESS.jpg" },
		{ title: "Dr Pepper Tank Top", title_ar: "در بيبر توب", image: "tops/342027010_CLASSIC-DR-PEPPER-TANK.jpg" },
		{ title: "Uptown Studios Tank Top", title_ar: "ابتاون ستديوس توب", image: "tops/342170772_UPTOWN-STUDIOS-ALICE-TANK.jpg" }
	],
	bottoms: [
		{ title: "Fray Hem Skinny Jeans", title_ar: "سكيني جينز", image: "bottoms/340581340_ROSSO-DROP-DOWN-HEM.jpg" },
		{ title: "Leather Wrap Mini Skirt", title_ar: "تنورة جلد", image: "bottoms/318488001_SCALLOP-SUIT-SHORT.jpg" },
		{ title: "Midi Skater Skirt", title_ar: "تنورة سكيتر ميدي", image: "bottoms/326219988_TEXTURE-WAFFLE-BALLOON-MIDI.jpg" },
		{ title: "Print Waist Trousers", title_ar: "بنطلون بالطباعة", image: "bottoms/332743849_AMARES-TILE-JSY-ELASTIC-JOGGER.jpg" },
		{ title: "High Waisted Tube Skirt", title_ar: "تنورة ضيقة قصيرة", image: "bottoms/336368775_L.I.-CE-TUBE.jpg" },
		{ title: "Wrap Maxi Skirt", title_ar: "تنورة ماكسي", image: "bottoms/WrapMaxiSkirt.jpg" },
		{ title: "Angular Stripe Pencil Skirt", title_ar: "تنورة قلم الرصاص", image: "bottoms/337669509_EC-CUTABOUT-STRIPE-PENCIL.jpg" },
		{ title: "Aztec Print Trousers", title_ar: "بنطلون بالطباعة الازتيك", image: "bottoms/338507709_EC-MONTANA-VERTICAL-JSY-TRS.jpg" },
		{ title: "Cord Mini Skirt", title_ar: "تنورة قصيرة", image: "bottoms/339943417_CORD-SKIRT.jpg" },
		{ title: "Plain Joggers", title_ar: "لجينجس", image: "bottoms/342360301_PLAIN-VE-JOGGER.jpg" }
	],
	jackets: [
		{ title: "Plain Waterfall Cardigan", title_ar: "سترة زيتي", image: "jackets/328103334_T-WATERFALL-CARDI.jpg" },
		{ title: "3/4 Sleeve Blazer", title_ar: "سترة وردي", image: "jackets/329093265_SS15-TIRAMISU-BLAZER.jpg" },
		{ title: "Sheer Stripe Kimono Cardigan", title_ar: "كيمونو كارديجان", image: "jackets/329568111_THICK-+-THIN-KIMONO-CARDI.jpg" },
		{ title: "Floral Print Kimono", title_ar: "كيمونو زهري", image: "jackets/330128304_LORNA-FLORAL-DROP-PKT-KIMONO.jpg" },
		{ title: "Denim Jacket", title_ar: "سترة جينز", image: "jackets/330558840_ARIEL-DENIM-JACKET.jpg" },
		{ title: "Print Drop Pocket Kimono", title_ar: "كيمونو بالطباعة", image: "jackets/331645049_DEENA-MIX-DRP-PKT-KIMONO.jpg" },
		{ title: "Print Waterfall Blazer", title_ar: "سترة بالطباعة", image: "jackets/334101649_SKYLER-PRINT-WATERFALL-BLAZER.jpg" },
		{ title: "Stripe Waterfall Blazer", title_ar: "سترة مونوكروم", image: "jackets/334102049_STRIPE-WATERFALL-ZIP-BLAZER.jpg" },
		{ title: "Oriental Print Kimono", title_ar: "أورينتال كيمونو", image: "jackets/340146449_T-STEPH-PAISLEY-LLINE-LS-KIM.jpg" }
	],
	accessories: [
		{ title: "Ankle Strap Heels", title_ar: "الكعب الليموني", image: "accessories/336300735SENSATORY-2---A.STRAP-DRESSY-9.jpg" },
		{ title: "Woven Spike Necklace", title_ar: "عقد بالسبايك", image: "accessories/334387809_GB-WOVEN-SPIKE-NL.jpg" },
		{ title: "Embellished Sandals", title_ar: "الصندل المزين", image: "accessories/335328514_WF-GLACIER---EMBELLISHED-SAND.jpg" },
		{ title: "Chunky Caged Platform Heels", title_ar: "صندل شنكي", image: "accessories/335373814_VITA---GLADIATOR-PLATFORM-13.0.jpg" },
		{ title: "Stripe Espadrilles", title_ar: "أحذية السترابيس", image: "accessories/335949040_WF-MARVEL---STRIPE-WOW-ESP.jpg" },
		{ title: "Beaded Necklace", title_ar: "قلادة من الخرز", image: "accessories/336370699_GS-CHAIN-AND-BEAD-NL.jpg" },
		{ title: "Ankle Strap Cork Wedges", title_ar: "الكعب الويدج", image: "accessories/336865614_PARFAIT--EMBELLISHED-CRK-WEDGE.jpg" },
		{ title: "Black Gem Bracelet", title_ar: "اسوارة بالجوهرة السوداء", image: "accessories/336957201_NL-JELLY-TOT-TRI-STRETCH-BLET.jpg" },
		{ title: "Ankle Strap Platform Heels", title_ar: "الكعب الشنكي", image: "accessories/339129801_TARINE---A.STRAP-PLPATFORM.jpg" },
		{ title: "Cut Out Side Platform Heels", title_ar: "الكعب الوردي", image: "accessories/339153477_TRACKSUIT---DORSAY-PLATFORM-13.jpg" },
		{ title: "Floral Print Headscarf", title_ar:"عقال بالأزهار", image: "accessories/339234179_BOTANICAL-FLORAL-WIRE.jpg" },
		{ title: "Ribboned Bow Sandals", title_ar: "صندل أنيق", image: "accessories/339635954_RIBBONED---BOW-SANDAL-10.jpg" },
		{ title: "Teardrop Diamante Ring", title_ar: "خاتم الدمعة", image: "accessories/340325410_70-OS-TEARDROP-RING.jpg" }
	]
};

export default content;