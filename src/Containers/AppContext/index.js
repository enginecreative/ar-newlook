import React, { Component } from 'react';

export default class AppContext extends Component {

	// receives state and dispatch from 'App'
	static propTypes = {
		state: React.PropTypes.object,
		onDispatch: React.PropTypes.func
	};

	// makes app state and dispatch available
	// to all components via context
	static childContextTypes = {
		state: React.PropTypes.object,
		dispatch: React.PropTypes.func
	};

	getChildContext() {
		return {
			state: this.props.state,
			dispatch: (action) => this.props.onDispatch(action)
		}
	}

	render() {
		return this.props.children
	}

}