import React, { Component } from 'react';

import Splash from '../../Components/Splash';
import GarmentSelector from '../../Components/GarmentSelector';
import MyLook from '../../Components/MyLook';

let AppView = () => {
	return (
	  <div>
		  <Splash></Splash>
		  <MyLook></MyLook>
		  <GarmentSelector></GarmentSelector>
	  </div>
	);
};

export default AppView;