import React, { Component } from 'react';

import AppContext from '../AppContext';
import AppView from '../AppView';

import ImageCompositor from '../../canvas/imageCompositor';

export default class App extends Component {
	constructor(props, context) {
		super(props, context);

		// App is the global state
		this.state = {
			hasOnboarded: false,
			isSelectingGarment: false,
			garmentType: '',
			jackets: null,
			tops: null,
			bottoms: null,
			accessories: null,
			isValidLook: true
		};

		this.compositor = new ImageCompositor();
	}

	checkIsValidLook = () => {
		if (this.state.jackets && this.state.tops && this.state.bottoms && this.state.accessories) {
			return true;
		}

		return false;
	};

	composeImage = () => {
		this.compositor.shareImages([`../img/${this.state.jackets.image}`, `../img/${this.state.tops.image}`, `../img/${this.state.bottoms.image}`, `../img/${this.state.accessories.image}`]);
	};

	handleDispatch(action) {
		if (action.type === 'START') {
			// Use Object.assign to return a new object rather than mutating
			this.setState(Object.assign({}, this.state, {hasOnboarded: true}));
		} else if (action.type === 'LAUNCH_GARMENT') {
			console.log('launching garment');
			this.setState(Object.assign({}, this.state, {isSelectingGarment: !this.state.isSelectingGarment, garmentType: action.garment}), _ => {
				console.log(this.state);
			});
		} else if (action.type === 'ADD_GARMENT') {
			this.setState(Object.assign({}, this.state, {isSelectingGarment: false, garmentType: null, [action.garmentType]: action.garment}), () => {
				console.log(this.state);
			});
		} else if (action.type === 'SHARE') {
			// Handle share action
			if (this.checkIsValidLook()) {
				this.setState(Object.assign({}, {isValidLook: true}));
				this.composeImage();
			} else {
				this.setState(Object.assign({}, {isValidLook: false}));
			}
		} else if (action.type === 'SUBMIT_ENTRY') {
			// Handle entry submit
			if (this.checkIsValidLook()) {
				this.setState(Object.assign({}, {isValidLook: true}));
			} else {
				this.setState(Object.assign({}, {isValidLook: false}));
			}
		}
	}

	render() {
		// new state gos to 'AppContext'
		return (
		  <AppContext
		  	state={this.state}
		  	onDispatch={(action) => this.handleDispatch(action)}>
			<AppView></AppView>
		  </AppContext>
		)
	}

}