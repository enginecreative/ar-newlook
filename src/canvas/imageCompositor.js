export default class ImageCompositor {

	constructor(imageUrls) {
		// Setup canvas
		this.canvasWidth = 640;
		this.canvasHeight = 1136;
		this.branding = document.createElement('img');
		this.canvas = document.createElement('canvas');
		this.context = this.canvas.getContext('2d');
		this.context.globalCompositeOperation = 'source-over';

		// Set branding image source
		this.branding.src = '../img/branding.png';

		// use arguments to add images to array
		this.images = imageUrls;
	}

	render(images) {
		// Clear the canvas
		this.canvas.width = this.canvasWidth;
		this.canvas.height = this.canvasHeight;

		// Fill with white
		this.context.fillStyle = 'rgba(255,255,255, 0)';
		this.context.fillRect(0,0,this.canvasWidth,this.canvasHeight);

		// Draw the branding underneath
		this.context.drawImage(this.branding, 0,0, this.branding.naturalWidth, this.branding.naturalHeight);

		// Loop through the images and add them to the canvas
		images.forEach((imgUrl, ind) => {

			let img = document.createElement('img');
			img.src = imgUrl;
			let itemsInRow = 2;
			let yAdjust = 220;
			let scale = ((this.canvasWidth / itemsInRow) / img.naturalWidth);
			let xOffset = (ind * (this.canvasWidth / itemsInRow)) % this.canvasWidth;
			let yOffset = (Math.ceil((ind+1) / itemsInRow)) * (scale * img.naturalHeight) - (scale * img.naturalHeight);
			let imageDrawWidth = (scale * img.naturalWidth) * 0.8;
			let imageDrawHeight = (scale * img.naturalHeight) * 0.8;

			this.context.drawImage(img, xOffset + (imageDrawWidth * 0.1), (yOffset + yAdjust)  + (imageDrawWidth * 0.1), imageDrawWidth, imageDrawHeight);
		});
	}

	shareImages(images) {
		// update
		this.render(images);

		let dataUrl = this.canvas.toDataURL();
		window.location.href = 'architectsdk://realityengine/share/image?'+encodeURI(dataUrl);
	}
}