import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import App from './Containers/App';

// Import CSS
require('./theme/newlook.scss');

ReactDOM.render(<App />, document.getElementById('app'));