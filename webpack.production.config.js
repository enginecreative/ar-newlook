var path = require('path');
var node_modules_dir = path.resolve(__dirname, 'node_modules');

var config = {
  entry: path.resolve(__dirname, 'src/main.js'),
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: "./",
    filename: 'main.js'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        include: path.join(__dirname, 'src'),
        loader: 'babel-loader',
        query: {
          presets: ['es2015', 'react', 'stage-0']
        }
      },
      {
        test: /\.scss/,
        loader: "style!css!autoprefixer!sass"
      },
      { test: /\.(png|jpg|gif)$/, loader: 'url-loader?limit=8192&root=.' }, // inline base64 URLs for <=8k images, direct URLs for the rest
      { test: /\.css$/, loader: 'style!css' }, // Only .css files // Run both loaders }
      { test: /\.woff$/, loader: 'url?limit=100000' }
    ]
  }
};

module.exports = config;