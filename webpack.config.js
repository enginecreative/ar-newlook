var path = require('path');
var webpack = require('webpack');

module.exports = {
	entry: [
		'babel-polyfill',
		'./src/theme/main.scss',
		'./src/main',
		'webpack-dev-server/client?http://localhost:8080'
	],
	output: {
		publicPath: '/',
		filename: 'main.js'
	},
	debug: true,
	devtool: 'source-map',
	module: {
		loaders: [
			{
				test: /\.js$/,
				include: path.join(__dirname, 'src'),
				loader: 'babel-loader',
				query: {
					presets: ['es2015', 'react', 'stage-0']
				}
			},
			{
				test: /\.scss/,
				loader: "style!css!autoprefixer!sass"
			},
			{ test: /\.(png|jpg|gif)$/, loader: 'url-loader?limit=8192&root=.' }, // inline base64 URLs for <=8k images, direct URLs for the rest
			{ test: /\.css$/, loader: 'style!css' }, // Only .css files // Run both loaders }
			{ test: /\.woff$/, loader: 'url?limit=100000' }
		]
	},
	devServer: {
		contentBase: "./src"
	}
};